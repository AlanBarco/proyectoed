/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.resources;

/**
 *
 * @author Marco
 */
public class ParametrosVentana {
    //tamaño de las ventanas
    public static final double HEIGHT_WINDOW = 700.0;
    public static final double WIDTH_WINDOW = 500.0;
    
    //Titulos
    public static final String TITTLE = "Sistema de Turnos";
    public static final String TITTLE_FORM = "Informacion del paciente";
    
    //Espaciado de botones y labels
    public static final double SPACING_BUTTONS = 35;
    public static final double SPACING_BUTTONS_VIDEO = 100;
    public static final double SPACING_LABEL_TEXT = 60;
    public static final double SPACING_VERTICAL = 10;
    
    //Espaciado de la tabla
    public static final double SPACING_LEFT_TABLE = 430.0;

    //metodos getter de las variables
    public static double getHEIGHT_WINDOW() {
        return HEIGHT_WINDOW;
    }

    public static double getWIDTH_WINDOW() {
        return WIDTH_WINDOW;
    }

    public static String getTITTLE() {
        return TITTLE;
    }

    public static String getTITTLE_FORM() {
        return TITTLE_FORM;
    }

    public static double getSPACING_BUTTONS() {
        return SPACING_BUTTONS;
    }

    public static double getSPACING_BUTTONS_VIDEO() {
        return SPACING_BUTTONS_VIDEO;
    }

    public static double getSPACING_LABEL_TEXT() {
        return SPACING_LABEL_TEXT;
    }

    public static double getSPACING_VERTICAL() {
        return SPACING_VERTICAL;
    }

    public static double getSPACING_LEFT_TABLE() {
        return SPACING_LEFT_TABLE;
    }
    
    
    
}
