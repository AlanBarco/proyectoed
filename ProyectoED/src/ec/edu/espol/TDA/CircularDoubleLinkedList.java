/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.TDA;

import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 *
 * @author alan_
 */
public class CircularDoubleLinkedList<E> implements List<E> {
    
    private Node<E> last;
    private int efective;
    
    public CircularDoubleLinkedList(){
        last=null;
        efective=0;
    }

    @Override
    public boolean addFirst(E element) {
        if (element == null) {
            return false;
        }
        Node<E> node = new Node<>(element);
        if (isEmpty()) {
            last = node;
            node.setNext(node);
            node.setPrevious(node);
        } else {
            node.setNext(last.getNext());
            last.getNext().setPrevious(node);
            node.setPrevious(last);
            last.setNext(node);
        }
        efective++;
        return true;
    }

    @Override
    public boolean addLast(E element) {
        if(element==null)
            return false;
        Node<E> node = new Node<>(element);
        if(isEmpty()){
            last=node;
            node.setNext(node);
            node.setPrevious(node);
        }else{
            Node<E> first = last.getNext();
            last.setNext(node);
            node.setPrevious(last);
            last = node;
            node.setNext(first);
            first.setPrevious(node);
        }
        efective++;
        return true;
    }

    @Override
    public boolean removeFirst() {
        if(isEmpty())
            return false;
        if(efective==1){
            last.setData(null);
            last=null;
        }else{
            Node<E> newFirst = last.getNext().getNext();
            last.getNext().setPrevious(null);
            last.getNext().setNext(null);
            last.setNext(newFirst);
            newFirst.setPrevious(last);
        }
        efective--;
        return true;
    }

    @Override
    public boolean removeLast() {
        if(isEmpty())
            return false;
        if(efective==1){
            last.setData(null);
            last=null;
        }else{
            Node<E> newLast = last.getPrevious();
            newLast.setNext(last.getNext());
            last.getNext().setPrevious(newLast);
            last.setNext(null);
            last.setPrevious(null);
            last = newLast;  
        }
        efective--;
        return true;
    }

    @Override
    public E getFirst() {
        if(isEmpty()){
            return null;
        }
        else{
            return last.getNext().getData();
        }
    }

    @Override
    public E getLast() {
        if(isEmpty()){
            return null;
        }
        else{
            return last.getData();
        }    }

    @Override
    public boolean insert(int index, E element) {
        if (index > efective - 1 || element == null || index < 0) {
            return false;
        }
        if (index == 0) {
            addFirst(element);
            return true;
        } 
        Node<E> node = new Node<>(element);
        int i = 0;
        Node<E> traveler = last.getNext();
        do {
            if (i+1==index) {
                node.setNext(traveler.getNext());
                traveler.getNext().setPrevious(node);
                traveler.setNext(node);
                node.setPrevious(traveler);
            }
            traveler = traveler.getNext();
            i++;
        } while (traveler != last.getNext());
        efective++;
        return true;
    }

    @Override
    public boolean contains(E element) {
        if(isEmpty() || element==null)
            return false;
        Node<E> traveler = last.getNext();
        if(last.getData().equals(element))
            return true;
        else{
        do{
            if(traveler.getData().equals(element)){
                return true;
            }
            traveler=traveler.getNext();
        }while(traveler!=last.getNext());
        }
        return false;
    }

    @Override
    public E get(int index) {
        Node<E> viajero = last.getNext();
        int i = 0;
        if (isEmpty() || efective < 0 || index > efective - 1) {
            return null;
        } else {
            do {
                viajero = viajero.getNext();
                i++;
            } while (i != index);
        }
        return viajero.getData();  
    }

    @Override
    public int indexOf(E element) {
        int i = 0;
        if(isEmpty() || element==null)
            return -1;
        else{
            Node<E> traveler = last.getNext();
            do{ 
                if(traveler.getData().equals(element))
                    return i;
                traveler=traveler.getNext();
                i++;
            }while(traveler!=last.getNext());
        }
        return -1;
    }

    @Override
    public boolean isEmpty() {
        return efective==0;
    }

    @Override
    public E remove(int index) {
        int i=0;
        Node<E> traveler = last.getNext();
        if(isEmpty() || index<0|| index>efective-1)
            return null;
        if(index==0){
            E data = last.getNext().getData();
            removeFirst();
            return data;
        }
        else if(index==efective-1){
            E data = last.getData();
            removeLast();
            return data;
        }
        else{
            do{
                traveler=traveler.getNext();
                i++;
            }while(i!=index);
            Node<E> tmp = traveler.getPrevious();
            tmp.setNext(traveler.getNext());
            last.setPrevious(tmp);
            traveler.setNext(null);
            traveler.setPrevious(null);
            traveler.setData(null);
        }
        efective--;
        return traveler.getData();   
    }

    @Override
    public boolean remove(E element) {
        if(isEmpty() || element==null)
            return false;
        else {
            Node<E> traveler = last.getNext();
            do {
                if (traveler.getData().equals(element)) {
                    Node<E> tmp = traveler.getPrevious();
                    tmp.setNext(traveler.getNext());
                    traveler.getNext().setPrevious(tmp);
                    traveler.setNext(null);
                    traveler.setPrevious(null);
                    traveler.setData(null);
                    efective--;
                    return true;
                }else{
                    traveler = traveler.getNext();
                }

            } while (traveler != last.getNext());
        }
        return false;
    }

    @Override
    public E set(int index, E element) {
        if(isEmpty() || element==null|| index>efective-1 || index<0)
            return null;
        int i = 0;
        Node<E> traveler = last.getNext();
        do{
            if(i==index){
                E data = traveler.getData();
                traveler.setData(element);
                return data;
            }
            traveler=traveler.getNext();
            i++;
            
        }while(i!=index);
        return null;
    }

    @Override
    public int size() {
        return efective;
    }
    @Override
    public String toString() {
        if(!isEmpty()){
        StringBuilder s = new StringBuilder();
        s.append("[");
        Node<E> node = last.getNext();
        do {
            if (node == last) {
                s.append(node.getData());
            } else {
                s.append(node.getData() + ",");
            }
            node = node.getNext();
        } while (node != last.getNext());
        s.append("]");
        return s.toString();
        }
        return "";
    }
    
    public ListIterator<E> listIterator() {
        ListIterator<E> iterator = new ListIterator<E>() {
            private Node<E> p = last;
            @Override
            public boolean hasNext() {
                return p!=null;
            }

            @Override
            public E next() {
                if(!hasNext()){
                    throw new NoSuchElementException();
                }
                E data = p.getData();
                p=p.getNext();
                return data;
            }

            @Override
            public boolean hasPrevious() {
                return p.getPrevious()!=null;
            }

            @Override
            public E previous() {
                Node<E> previo = p.getPrevious();
                E data = previo.getData();
                p=previo.getPrevious();
                return data;
            }

            @Override
            public int nextIndex() {
                return 0;
            }

            @Override
            public int previousIndex() {
                return 0;
            }

            @Override
            public void remove() {
                //Do nothing yet
            }

            @Override
            public void set(E e) {
                //No hace nada aun
                
            }

            @Override
            public void add(E e) {
                throw new UnsupportedOperationException();
               
            }

        };
        return iterator;
    }
}
