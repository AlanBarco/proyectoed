/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.TDA;

/**
 *
 * @author alan_
 */
public class Medico {
    private String nombre;
    private Paciente paciente;
    private int puesto;
    private boolean disponible;

    public Medico(String nombre, Paciente paciente, int puesto, boolean disponible) {
        this.nombre = nombre;
        this.paciente = paciente;
        this.puesto = puesto;
        this.disponible = disponible;
    }

    public Medico(int puesto, String nombre, boolean disponible) {
        this.nombre = nombre;
        this.puesto = puesto;
        this.disponible = disponible;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

   @Override
    public String toString() {
	return puesto + "|" + nombre + "|" + disponible;
    }
}
