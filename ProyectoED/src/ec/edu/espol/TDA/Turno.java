/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.TDA;

/**
 *
 * @author alan_
 */
public class Turno {
    private String paciente;
    private String puesto;

    public Turno(String paciente, String puesto) {
        this.paciente = paciente;
        this.puesto = puesto;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    @Override
    public String toString() {
        return  paciente + "|" + puesto;
    }
    
    
}
