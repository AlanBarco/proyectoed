/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.methods;

import ec.edu.espol.TDA.CircularDoubleLinkedList;
import ec.edu.espol.TDA.Medico;
import ec.edu.espol.TDA.Paciente;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.Queue;
import java.util.PriorityQueue;

import java.util.LinkedList;

/**
 *
 * @author Marco
 */
public class Metodos {
    private Metodos(){
        
    }
    
    private static CircularDoubleLinkedList<String> listVideos = new CircularDoubleLinkedList<>();
	private final static File fileVideos = new File("src/ec/edu/espol/methods/nombrevideos.txt");

	private final static File fileSintomas = new File("src/ec/edu/espol/methods/sintomasexistentes.txt");

	private final static File filePaciente = new File("src/ec/edu/espol/methods/nombrepacientes.txt");
	
	private final static File filePuestos = new File("src/ec/edu/espol/methods/puestos.txt");
        	
	private static Scanner sc;
        
        /*------------------------------METODOS------------------------------*/
        //Metodo para agregar un nuevo video
        public static CircularDoubleLinkedList<String> addVideo(String nombreVideo) {
            try(FileWriter fw  = new FileWriter(fileVideos,true)){
			fw.write(nombreVideo);
			fw.close();
		} catch (Exception e) {
                    Logger.getLogger(Metodos.class.getName()).log(Level.SEVERE, null, e);  
                }
		return getListWithUrls();
	}
        
        //Añadir los videos a la LinkedList
        public static CircularDoubleLinkedList<String> getListWithUrls() {
           try(Scanner scanner = new Scanner(fileVideos)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				listVideos.addLast("src/ec/edu/espol/resources/" + line);
			}
		} catch (FileNotFoundException e) {
			Logger.getLogger(Metodos.class.getName()).log(Level.SEVERE, null, e);
		}
            
		return listVideos;
	}
        
        //agregar un nuevo paciente
        public static boolean addPaciente(Paciente infoPaciente) {
		try(FileWriter fw  = new FileWriter(filePaciente,true)){
			fw.write(infoPaciente.toString() + "\n");
			fw.close();
		} catch (Exception e) {
                    Logger.getLogger(Metodos.class.getName()).log(Level.SEVERE, null, e);
			return false;
		}
		return true;
	}
        
        //borrar un paciente
        public static void liberarPaciente(String nombrePaciente) {
            StringBuilder sb = new StringBuilder();
            try(Scanner scanner = new Scanner(filePaciente)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] sp = line.split("\\|");
				if(!sp[0].equals(nombrePaciente))
					sb.append(line + "\n");
			}
            } catch (Exception e) {
                    Logger.getLogger(Metodos.class.getName()).log(Level.SEVERE, null, e);
            }
                
            try (FileWriter fw = new FileWriter(filePaciente, false)) {
                sc = new Scanner(filePaciente);
                fw.write(sb.toString());
                fw.close();
            } catch (Exception e) {
                Logger.getLogger(Metodos.class.getName()).log(Level.SEVERE, null, e);
            } finally {
                sc.close();
            }
        }
        
        //obtener pacientes de una cola segun prioridad que tenga un sintoma
        public static PriorityQueue<Paciente> getColaPaciente() {
		LinkedList<Paciente> listaPacientes = new LinkedList<Paciente>();
		try {
			sc = new Scanner(filePaciente);
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				String[] sp = line.split("\\|");
				listaPacientes.add(new Paciente(sp[0], sp[1], sp[2], sp[3], sp[4], Integer.valueOf(sp[5])));
			}
		} catch (Exception e) {
                    Logger.getLogger(Metodos.class.getName()).log(Level.SEVERE, null, e);
		}
		PriorityQueue<Paciente> cola = new PriorityQueue<Paciente>(
				(Paciente p1, Paciente p2) -> p1.getPrioridad() - p2.getPrioridad());
		cola.addAll(listaPacientes);
		return cola;
	}
        
        //metodo que genera una cola para atender a los pacientes por prioridad
        public static int prioridad(String sintoma) {
		int priority = 0;
		try(Scanner scanner = new Scanner(fileSintomas)) {
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				String[] sp = line.split("\\|");
				if (sp[0].equals(sintoma))
					return Integer.valueOf(sp[1]);
			}
		} catch (Exception e) {
                    Logger.getLogger(Metodos.class.getName()).log(Level.SEVERE, null, e);
                }
		return priority;
	}
       
        //obtener puesto del medico
        public static Queue<Medico> getPuesto() {
		Queue<Medico> puestos = new LinkedList<>();
		try {
			sc = new Scanner(filePuestos);
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				String[] sp = line.split("\\|");
				puestos.add(new Medico(Integer.valueOf(sp[0]), sp[1], Boolean.valueOf(sp[2])));
			}
		} catch (Exception e) {
                    Logger.getLogger(Metodos.class.getName()).log(Level.SEVERE, null, e);
		}
		return puestos;
	}
	
        //agregar un puesto al medico
	public static void addPuesto(Medico information) {
		try(FileWriter fw  = new FileWriter(filePuestos,true)){
			fw.write(information.toString()+ "\n");
			fw.close();
		} catch (Exception e) {
                    Logger.getLogger(Metodos.class.getName()).log(Level.SEVERE, null, e);
		}
	}
}
