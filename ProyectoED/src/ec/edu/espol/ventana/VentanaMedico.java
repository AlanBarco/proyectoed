/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.ventana;
import ec.edu.espol.TDA.Medico;
import ec.edu.espol.resources.ParametrosVentana;
import ec.edu.espol.methods.Metodos;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
/**
 *
 * @author alan_
 */
public class VentanaMedico extends Application{
	private AnchorPane root = new AnchorPane();
	private HBox hbNombre = new HBox();
	private HBox hbPuesto = new HBox();
	private Label lbNombre = new Label("Nombre");
	private Label lbPuesto = new Label("Puesto");
	private TextField nombre = new TextField();
	private TextField puesto = new TextField();
	private Button volver = new Button("Volver");
	private Button registrar = new Button("Registrar");

        
        
	@Override
	public void start(Stage stage) {
                nombre.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[a-z A-Z]+"))? change:null));
                puesto.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[0-9 .]+"))? change:null));
		manageHBox(hbNombre, lbNombre, nombre);
		manageHBox(hbPuesto, lbPuesto, puesto);
		AnchorPane.setLeftAnchor(hbNombre, 20.0);
		AnchorPane.setTopAnchor(hbNombre, 20.0);
		AnchorPane.setLeftAnchor(hbPuesto, 20.0);
		AnchorPane.setTopAnchor(hbPuesto, 60.0);

                
		volver.setOnAction((ActionEvent e) -> {
			VentanaPrincipal returnOptions = new VentanaPrincipal();
			returnOptions.start(stage);
		});

		registrar.setOnAction((ActionEvent e) -> {
                        if(nombre.getText().isEmpty()|| puesto.getText().isEmpty()){
                            Alert a=new Alert(Alert.AlertType.ERROR,"No dejar campos vacios");
                            a.show();
                        }
                        else{
			Medico medico = new Medico(Integer.valueOf(puesto.getText()), nombre.getText(), false);
			Metodos.addPuesto(medico);
                        Alert a=new Alert(Alert.AlertType.CONFIRMATION,"Medico registrado con éxiro");
                        a.show();
			cleanText();
                        }
		});
				

		AnchorPane.setLeftAnchor(registrar, 20.0);
		AnchorPane.setBottomAnchor(registrar, 20.0);
		AnchorPane.setLeftAnchor(volver, 150.0);
		AnchorPane.setBottomAnchor(volver, 20.0);
                
                Image image1 = new Image(getClass().getResourceAsStream("fondoprincipal.jpg"));
                ImageView imageView1 = new ImageView(image1);
                
		root.getChildren().addAll(imageView1,volver, registrar, hbNombre, hbPuesto);

		Scene scene = new Scene(root, ParametrosVentana.getHEIGHT_WINDOW(), ParametrosVentana.getWIDTH_WINDOW());
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.setResizable(true);

	}
    private void manageHBox(HBox hbox, Label label, TextField textfield) {
        label.setTextFill(Color.YELLOW);
        hbox.getChildren().addAll(label, textfield);
        hbox.setSpacing(ParametrosVentana.getSPACING_LABEL_TEXT());
    }
    
    private void cleanText() {
    	nombre.clear();
    	puesto.clear();
    }
}
