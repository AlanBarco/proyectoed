/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.ventana;

import javafx.scene.control.ComboBox;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import ec.edu.espol.TDA.Paciente;
import ec.edu.espol.methods.Metodos;
import ec.edu.espol.resources.ParametrosVentana;
import javafx.scene.control.Alert;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

import javafx.scene.control.TextField;
import javafx.scene.control.Label;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;


/**
 *
 * @author alan_
 */
public class VentanaPaciente extends Application {
    private AnchorPane root = new AnchorPane();
    private VBox vform = new VBox();
    ObservableList<String> list = FXCollections.observableArrayList("Masculino", "Femenino");
    private ComboBox<String> genero = new ComboBox<>(list);
    private TextField nombres = new TextField();
    private TextField apellidos = new TextField();
    private TextField edad = new TextField();
    private TextField sintoma = new TextField();
    private HBox hbNombres = new HBox();
    private HBox hbApellidos = new HBox();
    private HBox hbEdad = new HBox();
    private HBox hbGenero = new HBox();
    private HBox hbSintomas = new HBox();
    private Label lbNombres = new Label("Nombres:");
    private Label lbApellidos = new Label("Apellidos:");
    private Label lbEdad = new Label("Edad:");
    private Label lbGenero = new Label("Genero:");
    private Label lbSintomas = new Label("Sintomas:");
    private Button añadirP = new Button("Registrar");
    private Button regresar = new Button("Volver");

    
    
    @Override
    public void start(Stage stage) {
        nombres.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[a-z A-Z]+"))? change:null));
        apellidos.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[a-z A-Z]+"))? change:null));
        edad.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[0-9]+"))? change:null));
        sintoma.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[a-z A-Z]+"))? change:null));
        manageHBox(hbNombres, lbNombres, nombres);
        manageHBox(hbApellidos, lbApellidos, apellidos);
        manageHBox(hbEdad, lbEdad, edad);
        manageHBox(hbGenero, lbGenero, genero);
        manageHBox(hbSintomas, lbSintomas, sintoma);
        vform.getChildren().addAll(hbNombres, hbApellidos,hbGenero, hbEdad, hbSintomas);
        vform.setSpacing(ParametrosVentana.getSPACING_VERTICAL());
        AnchorPane.setLeftAnchor(vform, 20.0);
        AnchorPane.setTopAnchor(vform, 100.0);

        añadirP.setOnMouseClicked(e -> {
            if(nombres.getText().isEmpty()|| sintoma.getText().isEmpty()||apellidos.getText().isEmpty()||edad.getText().isEmpty()||sintoma.getText().isEmpty()){
                Alert a=new Alert(Alert.AlertType.ERROR,"No dejar campos vacios");
                a.show();
            }
            else{
            int priority = Metodos.prioridad(sintoma.getText());
            Metodos.addPaciente(new Paciente(nombres.getText(), apellidos.getText(),
                    edad.getText(), genero.getValue(), sintoma.getText(), priority));
               Alert a=new Alert(Alert.AlertType.CONFIRMATION,"Paciente registrado con éxito");
                a.show();
            cleanTextFields();
            
            }

        });

        regresar.setOnMouseClicked(e -> {
            VentanaPrincipal newWindows = new VentanaPrincipal();
            newWindows.start(stage);
        });

        AnchorPane.setLeftAnchor(añadirP, 20.0);
        AnchorPane.setBottomAnchor(añadirP, 20.0);

        AnchorPane.setLeftAnchor(regresar, 150.0);
        AnchorPane.setBottomAnchor(regresar, 20.0);
        
        Image image1 = new Image(getClass().getResourceAsStream("fondoprincipal.jpg"));
        ImageView imageView1 = new ImageView(image1);
        
        root.getChildren().addAll(imageView1,vform, añadirP, regresar);
        Scene scene = new Scene(root, ParametrosVentana.getHEIGHT_WINDOW(),
                ParametrosVentana.getWIDTH_WINDOW());
        stage.centerOnScreen();
        stage.setResizable(true);
        stage.setTitle(ParametrosVentana.getTITTLE_FORM());
        stage.setScene(scene);
    }
    
    private void manageHBox(HBox hbox, Label label, TextField textfield) {
        label.setTextFill(Color.YELLOW);
        hbox.getChildren().addAll(label, textfield);
        hbox.setSpacing(ParametrosVentana.getSPACING_LABEL_TEXT());
    }

    private void manageHBox(HBox hbox, Label label, ComboBox<String> combobox) {
        label.setTextFill(Color.YELLOW);
        hbox.getChildren().addAll(label, combobox);
        hbox.setSpacing(ParametrosVentana.getSPACING_LABEL_TEXT());
    }

    private void cleanTextFields() {
        nombres.clear();
        apellidos.clear();
        edad.clear();
        sintoma.clear();
    }
}
