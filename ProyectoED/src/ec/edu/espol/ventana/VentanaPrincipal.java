/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.ventana;

import ec.edu.espol.resources.ParametrosVentana;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.event.ActionEvent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 *
 * @author alan_
 */
public class VentanaPrincipal extends Application{
	private AnchorPane root = new AnchorPane();
	private Button addPaciente;
	private Button addDoctor;
	private Button reproductor;
	private Button diagnosticar;
	private HBox buttons = new HBox();
		
        
	@Override
	public void start(Stage stage) {
		addPaciente = new Button("Agregar Paciente ");
		addPaciente.setOnAction((ActionEvent action) -> {setVentana(1, stage);
		});
		
		addDoctor = new Button("Agregar Doctor");
		addDoctor.setOnAction((ActionEvent action) -> {
			setVentana(2, stage);
		});
		
		reproductor = new Button("Turnos");
		reproductor.setOnAction((ActionEvent action) -> {
			setVentana(3, stage);
		}) ;
		
		diagnosticar = new Button("Diagnosticar");
		diagnosticar.setOnAction((ActionEvent event) -> {
			setVentana(4, stage);
		});
		buttons.setSpacing(30.0);
		buttons.getChildren().addAll(addPaciente, addDoctor, reproductor, diagnosticar);
                
                Image image1 = new Image(getClass().getResourceAsStream("fondoprincipal.jpg"));
                ImageView imageView1 = new ImageView(image1);
		
                Label label = new Label("SISTEMA CLINICO ESPOL");
                label.setFont(new Font("Times New Roman",30));
                label.setTextFill(Color.YELLOW);
                
                AnchorPane.setLeftAnchor(label, 150.0);
		AnchorPane.setTopAnchor(label, 80.0);
                
		AnchorPane.setLeftAnchor(buttons, 120.0);
		AnchorPane.setBottomAnchor(buttons, 100.0);
		root.getChildren().addAll(imageView1,label,buttons);
		Scene scene = new Scene(root ,ParametrosVentana.getHEIGHT_WINDOW() ,
				ParametrosVentana.getWIDTH_WINDOW() );
        stage.setTitle(ParametrosVentana.getTITTLE());
        stage.setResizable(true);
        stage.centerOnScreen();
        stage.setScene(scene);
        stage.show();      
	}
	

    public void main (String[] args){
        launch(args);
    }
    
    private void setVentana(int ventana, Stage stage) {
    	switch (ventana) {
			case 1:
				VentanaPaciente formulariowindow = new VentanaPaciente();
				formulariowindow.start(stage);
				break;
			case 2:
				VentanaMedico medicowindow = new VentanaMedico();
				medicowindow.start(stage);
				break;
			case 3:
				VentanaTurnos reproductorwindow = new VentanaTurnos();
				reproductorwindow.start(stage);
				break;
			case 4:
				VentanaAtender diagnosticarwindow = new VentanaAtender();
				diagnosticarwindow.start(stage);
				break;
                        default:
                            break;
		}
    }
}
