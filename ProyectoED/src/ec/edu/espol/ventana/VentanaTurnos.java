/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.ventana;


import ec.edu.espol.TDA.CircularDoubleLinkedList;
import ec.edu.espol.TDA.Paciente;
import ec.edu.espol.TDA.Medico;
import ec.edu.espol.TDA.Turno;

import ec.edu.espol.methods.Metodos;
import ec.edu.espol.resources.ParametrosVentana;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.List;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;

import java.io.File;
import java.util.ListIterator;

import javafx.scene.control.Button;

import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;

import javafx.event.ActionEvent;

import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.collections.*;

import javafx.scene.media.MediaPlayer;
import javafx.scene.media.Media;
import javafx.scene.media.MediaView;
import javafx.util.Duration;
/**
 *
 * @author alan_
 */
public class VentanaTurnos extends Application {
    private AnchorPane root = new AnchorPane();
    private CircularDoubleLinkedList<String> urls = Metodos.getListWithUrls();
    private PriorityQueue<Paciente> queue = Metodos.getColaPaciente();
    private Queue<Medico> puestos = Metodos.getPuesto();
    private TableView<Turno> listaTurnos = new TableView<>();
    private TableColumn<Turno, String> cTurno = new TableColumn<>("Turno");
    private TableColumn<Turno, String> cPuesto = new TableColumn<>("Puesto");
    private ObservableList<Turno> data;
    private HBox buttons = new HBox();
    private Button siguiente;
    private Button previo;
    private Button volver;
    ListIterator<String> listIterator =urls.listIterator();
    Media media;
    MediaPlayer mp;
    MediaView mv;

    
    
    @SuppressWarnings("unchecked")
    @Override
    public void start(Stage stage) {
        changeVideo(listIterator);

        cTurno.prefWidthProperty().bind(listaTurnos.widthProperty().multiply(0.5));
        cPuesto.prefWidthProperty().bind(listaTurnos.widthProperty().multiply(0.5));

        cTurno.setResizable(false);
        cTurno.setCellValueFactory(new PropertyValueFactory<>("paciente"));
        cPuesto.setResizable(false);
        cPuesto.setCellValueFactory(new PropertyValueFactory<>("puesto"));
        listaTurnos.setItems(getTurnos());
        listaTurnos.getColumns().addAll(cTurno, cPuesto);

        AnchorPane.setLeftAnchor(listaTurnos, ParametrosVentana.getSPACING_LEFT_TABLE());

        siguiente = new Button("Siguiente Video");
        siguiente.setOnAction((ActionEvent e) -> {
            mp.setVolume(0);
            mp.stop();
            changeVideo(listIterator);
        });    

        previo = new Button("Video Anterior");
        previo.setOnAction((ActionEvent e) -> {
            mp.setVolume(0);
            mp.stop();
            changeVideo(listIterator);

        });

        volver = new Button("Volver");
        volver.setOnMouseClicked((e) -> {
            mp.setVolume(0);
            mp.stop();
            VentanaPrincipal options = new VentanaPrincipal();
            options.start(stage);
        });

        AnchorPane.setLeftAnchor(volver, 320.0);
        AnchorPane.setTopAnchor(volver, 450.0);

        buttons.setSpacing(ParametrosVentana.getSPACING_BUTTONS_VIDEO());
        buttons.getChildren().addAll(previo, siguiente, volver);

        AnchorPane.setTopAnchor(buttons, 420.0);
        AnchorPane.setLeftAnchor(buttons, 5.00);

        root.getChildren().addAll(buttons,listaTurnos);

        Scene scene = new Scene(root, ParametrosVentana.getHEIGHT_WINDOW(), ParametrosVentana.getWIDTH_WINDOW());
        stage.setTitle(ParametrosVentana.getTITTLE());
        stage.setResizable(false);
        stage.centerOnScreen();
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String args[]) {
        launch(args);
    }

    private ObservableList<Turno> getTurnos() {
        List<Turno> linked = new LinkedList<>();
        while (!queue.isEmpty()) {
            Paciente paciente = queue.poll();
            for (Medico medico : puestos) {
                if (!medico.isDisponible()) {
                    medico.setDisponible(true);
                    linked.add(new Turno(paciente.getNombres(), String.valueOf(medico.getPuesto())));
                    break;
                }
            }
        }
        data = FXCollections.observableList(linked);
        return data;
    }
        private void changeVideo(ListIterator<String> iterator) {
        media = new Media(new File(iterator.next()).toURI().toString());
        mp = new MediaPlayer(media);
        root.getChildren().remove(mv);
        mv = new MediaView(mp);
        mv.setMediaPlayer(mp);
        mv.setFitHeight(300);
        mv.setPreserveRatio(true);
        mv.setFitWidth(400);
        Runnable runnable_play = () -> { 
        	mp.seek(Duration.ZERO);
            mp.play();
        };
        mp.setOnReady(runnable_play);
        Runnable runnable_stop = () -> { 
        	changeVideo(listIterator);
        };
        mp.setOnEndOfMedia(runnable_stop);
        root.getChildren().add(mv);
    }
}
