/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.ventana;

import ec.edu.espol.TDA.Paciente;
import ec.edu.espol.methods.Metodos;
import ec.edu.espol.resources.ParametrosVentana;
import java.util.PriorityQueue;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;

import javafx.scene.layout.AnchorPane;

import javafx.event.ActionEvent;
import javafx.scene.control.Alert;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
/**
 *
 * @author alan_
 */
public class VentanaAtender extends Application{
	private AnchorPane root = new AnchorPane();
	private PriorityQueue<Paciente> colaP = Metodos.getColaPaciente();
	private Label lbNombre = new Label("Nombre Paciente:");
	private TextField nombre = new TextField();
	private Label lbDiagnostico = new Label("Diagnostico:");
	private TextField diagnostico = new TextField();
	private Button btDiagnosticar = new Button("Diagnosticar");
	private Button btVolver = new Button("Volver");
	
        
        
	@Override
	public void start(Stage stage) {
		try{Paciente paciente = colaP.poll();
                diagnostico.setTextFormatter(new TextFormatter<>(change ->
                (change.getControlNewText().matches("[a-z A-Z]+"))? change:null));
                
		btDiagnosticar.setOnAction((ActionEvent e) -> {paciente.setDiagnostico(diagnostico.getText());
                    if(diagnostico.getText().isEmpty()){
                        Alert a=new Alert(Alert.AlertType.ERROR,"No dejar campos vacios");
                        a.show();
                    }
                    else{
                    Metodos.liberarPaciente(paciente.getNombres());
                    Alert a=new Alert(Alert.AlertType.CONFIRMATION,"Diagnóstico exitoso");
                    a.show();
			clean();
                        
                    }
		});
		
		btVolver.setOnAction((ActionEvent e) -> {
			VentanaPrincipal options = new VentanaPrincipal();
			options.start(stage);
		});
		
		AnchorPane.setLeftAnchor(btDiagnosticar, 20.0);
		AnchorPane.setBottomAnchor(btDiagnosticar, 20.0);
		
		AnchorPane.setLeftAnchor(btVolver, 120.0);
		AnchorPane.setBottomAnchor(btVolver, 20.0);
		
		AnchorPane.setLeftAnchor(lbNombre, 20.0);
		AnchorPane.setTopAnchor(lbNombre, 20.0);
		
		nombre.setText(paciente.getNombres() + " " + paciente.getApellidos());
		nombre.setEditable(false);
		AnchorPane.setLeftAnchor(nombre, 190.0);
		AnchorPane.setTopAnchor(nombre, 20.0);
		
		AnchorPane.setLeftAnchor(lbDiagnostico, 20.0);
		AnchorPane.setTopAnchor(lbDiagnostico, 80.0);
		
		AnchorPane.setLeftAnchor(diagnostico, 190.0);
		AnchorPane.setTopAnchor(diagnostico, 80.0);
		
		root.getChildren().addAll(btDiagnosticar,btVolver, lbNombre, nombre,
				lbDiagnostico, diagnostico);

		Scene scene = new Scene(root, ParametrosVentana.getHEIGHT_WINDOW(), 
				ParametrosVentana.getWIDTH_WINDOW());
		stage.setScene(scene);
		stage.centerOnScreen();
		stage.setResizable(false);
                }catch(Exception e){
                    Alert a=new Alert(Alert.AlertType.INFORMATION,"No hay pacientes en turno");
                    a.show();
                }
	}
	
	private void clean() {
		nombre.clear();
		diagnostico.clear();
	}
}
